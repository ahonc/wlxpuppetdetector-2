<?php

include_once ('botclasses.php');
include_once ('basebot.php');
$wiki = new basebot("https://commons.wikimedia.org/w/api.php");
$wiki->quiet = 1;
$wiki->setUserAgent('User-Agent: BaseBot (https://meta.wikimedia.org/wiki/User:BaseBot)');
$wiki_mycnf = parse_ini_file("/data/project/basebot/wiki.my.cnf");
$wiki->login($wiki_mycnf['user'], $wiki_mycnf['password']);
$wiki->setLogin($login);

$images = $wiki->categorymembers_imageinfo_and_description(
        'Category:Images_from_Wiki_Loves_Monuments_2017_in_Ukraine', 50, '|metadata'
);

$parsed_images = array();
foreach ($images as $image) {
    $title = $image["title"];
    $fileinfo = end($image["imageinfo"]);
    $filedesc = $image["revisions"][0]["*"];
    preg_match_all('/{{\s*Monument[ _]Ukraine\s*\|(.*?)}}/ism', $filedesc, $matches);

    $sid = trim($matches[1][0]);
    $user = $fileinfo["user"];
    if (isset($fileinfo["metadata"]["Model"]) && isset($fileinfo["metadata"]["DateTimeOriginal"])) {
        $model = $fileinfo["metadata"]["Model"];
        $date = $fileinfo["metadata"]["DateTimeOriginal"];
    } else {
        continue;
    }
    $parsed_image = array();
    $parsed_image["title"] = $title;
    $parsed_image["sid"] = $sid;
    $parsed_image["user"] = $user;
    $parsed_image["model"] = $model;
    $parsed_image["date"] = $date;
    $parsed_images[] = $parsed_image;
    var_dump($parsed_image);
    unset($parsed_image);
    echo "<br><br>that was parsed image";
    break;
}
unset($images, $image);
var_dump($parsed_images);
